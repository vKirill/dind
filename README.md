#собрать dind
```
docker build -t dind .
```
#запустить после сборки
#\-\-cap-add=SYS_PTRACE для разрешения выполнения strace
```
docker run -ti --rm --privileged --cap-add=SYS_PTRACE --name some-docker -d dind
```
#после запуска зайти в some-docker
```
docker exec -ti some-docker bash
```

#зайти в папку уже склонированного репозитория для теста
```
cd nginx-docker
```

#собрать и запустить nginx
```
docker build -t nginx .
docker run -ti --rm --name web -d nginx
```
#проверка что nginx работает и отдает код 200 с запланированных текстом
```
curl -i 127.0.0.1:8089
```
