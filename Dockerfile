FROM debian:10

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
		ca-certificates	openssh-client apt-transport-https curl \
	        gnupg-agent software-properties-common gnupg wget procps \
		iptables openssl nano git e2fsprogs strace

RUN wget -O- https://download.docker.com/linux/debian/gpg | apt-key add -
RUN add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
               docker-ce docker-ce-cli containerd.io

RUN git clone https://github.com/zCirill/nginx-docker.git

COPY dockerd-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/dockerd-entrypoint.sh
ENV DOCKER_TLS_CERTDIR=/certs
RUN mkdir /certs /certs/client && chmod 1777 /certs /certs/client

EXPOSE 2375 2376

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
